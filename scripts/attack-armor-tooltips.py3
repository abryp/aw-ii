######### Heav, Medm, Lght, Unrd, Frtd, Hero
normal = [0.80, 1.00, 1.50, 1.00, 0.50, 1.00]
pierce = [0.80, 1.00, 1.00, 1.50, 0.35, 0.50]
magic  = [1.50, 1.25, 1.00, 1.00, 0.35, 0.70]
siege  = [1.00, 1.00, 1.00, 1.50, 1.50, 0.50]
chaos  = [1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
hero   = [1.00, 1.00, 1.00, 1.00, 0.50, 1.00]

heavy     = [normal[0], pierce[0], magic[0], siege[0], chaos[0], hero[0]]
medium    = [normal[1], pierce[1], magic[1], siege[1], chaos[1], hero[1]]
light     = [normal[2], pierce[2], magic[2], siege[2], chaos[2], hero[2]]
unarmored = [normal[3], pierce[3], magic[3], siege[3], chaos[3], hero[3]]
fortified = [normal[4], pierce[4], magic[4], siege[4], chaos[4], hero[4]]
hero_arm  = [normal[5], pierce[5], magic[5], siege[5], chaos[5], hero[5]]

attack_types = ["Normal", "Pierce", "Magic && Spells", "Siege", "Chaos", "Hero"]
armor_types = ["Heavy", "Medium", "Light", "Unarmored", "Fortified", "Hero"]

def armor(damage):
    if (damage == 1.00):
        return "100%"
    if (damage < 1.00):
        return "|c0000FF00" + str(int(damage * 100)) + "%|r"
    return "|c00FF0000" + str(int(damage * 100)) + "%|r"

def damage(armor):
    if (armor == 1.00):
        return "100%"
    if (armor < 1.00):
        return "|c00FF0000" + str(int(armor * 100)) + "%|r"
    return "|c0000FF00" + str(int(armor * 100)) + "%|r"

def armor_tooltip(armor_index):
    return ("Damage from Attack types:"
            "|n" + armor(normal[armor_index]) + " " + attack_types[0] +
            "|n" + armor(pierce[armor_index]) + " " + attack_types[1] +
            "|n" + armor( magic[armor_index]) + " " + attack_types[2] +
            "|n" + armor( siege[armor_index]) + " " + attack_types[3] +
            "|n" + armor( chaos[armor_index]) + " " + attack_types[4] +
            "|n" + armor(  hero[armor_index]) + " " + attack_types[5]
            )

def attack_tooltip(attack_index):
    return ("Damage to Armor types:"
            "|n" + damage(    heavy[attack_index]) + " " + armor_types[0] +
            "|n" + damage(   medium[attack_index]) + " " + armor_types[1] +
            "|n" + damage(    light[attack_index]) + " " + armor_types[2] +
            "|n" + damage(unarmored[attack_index]) + " " + armor_types[3] +
            "|n" + damage(fortified[attack_index]) + " " + armor_types[4] +
            "|n" + damage( hero_arm[attack_index]) + " " + armor_types[5]
            )

print("Attacks:")
for i in range(0,6):
    print(attack_types[i])
    print(attack_tooltip(i))

print("Armors:")
for i in range(0,6):
    print(armor_types[i])
    print(armor_tooltip(i))
